# -*- coding: utf-8 -*-

from . import db, app
from . import model
from flask import jsonify


def send_email():
    users = model.BettaEmail.query.filter(model.BettaEmail.user_id.is_(None)).all()
    for x in users:
        yield x.hash_link, x.email


@app.route('/user/<guid>', methods=['get'])
def get_user_status(guid):
    print(guid)
    user = model.BettaEmail.query.filter_by(hash_link=guid).first()
    print(user.__dict__)

    return jsonify(ok=True)