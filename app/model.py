# -*- coding: utf-8 -*-

from datetime import datetime
from . import db


class User(db.Model):
    user_id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(255), nullable=False, default='')
    last_name = db.Column(db.String(255), nullable=False, default='')
    password = db.Column(db.String(55), nullable=False, default='')
    email = db.Column(db.String(255), nullable=False, default='')
    email_confirm = db.Column(db.Boolean, nullable=False, default=False)
    phone = db.Column(db.String(20), nullable=False, default='')
    phone_confim = db.Column(db.Boolean, nullable=False, default=False)
    join_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    blocked_date = db.Column(db.DateTime, nullable=True)
    why_blocked = db.Column(db.Text, nullable=False, default='')


class Activity(db.Model):
    activity_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'), nullable=False)
    begin_timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    last_timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


class BettaEmail(db.Model):
    betta_email_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'), nullable=True)
    email = db.Column(db.String(255), nullable=False, default='')
    offer_data = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    hash_link  = db.Column(db.String(60), nullable=False, default='')

class BugReport(db.Model):
    bug_report_id = db.Column(db.Integer, primary_key=True)
    activity_id = db.Column(db.Integer, db.ForeignKey('activity.activity_id'), nullable=True)
    bug_report_timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    status = db.Column(db.String(20), nullable=False, default='')
    priorety = db.Column(db.Integer, nullable=False, default=5)
    report_text = db.Column(db.Text, nullable=False, default='')

class BugReportFile(db.Model):
    bug_report_file_id = db.Column(db.Integer, primary_key=True)
    bug_report_id = db.Column(db.Integer, db.ForeignKey('bug_report.bug_report_id'), nullable=False)
    file_index = db.Column(db.Integer, nullable=False, default=1)
    file_path = db.Column(db.String(255), nullable=False, default='')
    file_name = db.Column(db.String(255), nullable=False, default='')

class BugReportAnswer(db.Model):
    bug_report_answer_id = db.Column(db.Integer, primary_key=True)
    parent = db.Column(db.Integer, db.ForeignKey('bug_report_answer.bug_report_answer_id'), nullable=False)
    bug_report_id = db.Column(db.Integer, db.ForeignKey('bug_report.bug_report_id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'), nullable=False)
    ansver_text = report_text = db.Column(db.Text, nullable=False, default='')
    ansver_timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


def create_db():
    db.create_all()