#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import getopt
import sys


def print_help():
    print("Comming soon...")

if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:], "irh", ["initmodel", "runserver", "help", "send"])
    except getopt.GetoptError:
        print(help)
        sys.exit()
    print(opts, args, sys.argv)

    if not opts:
        print_help()

    for o, a in opts:
        print(o, a)
        if o in ["-i", "--initmodel"]:
            from app import model
            model.create_db()
        elif o in ["-r", "--runserver"]:
            from app import app, serv
            app.run(host='0.0.0.0', port=8080, debug=True)
        elif o == '--send':
            from app import serv
            print(list(serv.send_email()))
        elif o in ["-h", "--help"]:
            print_help()